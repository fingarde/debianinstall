#!/usr/bin/env bash
# -*- coding: utf-8 -*-
echo -e "\033[0;34mInstalling terminal for $user\033[0;m"

output="&1"
if [ ! -z ${use_script+x} ]; then mkdir logs 2> /dev/null; output="logs/terminal.log"; fi

apt install -y kitty

apt install -y powerline >> $output || exit 1
echo git clone https://github.com/powerline/fonts.git --depth=1 fonts >> $output || exit 1

fc-cache -f >> $output || exit 1

fonts/install.gsh >> $output || exit 1
rm -rf fonts >> $output || exit 1

ln -s $path/configs/kitty /home/$user/.config/kitty >> $output || exit 1

ln -s $path/configs/.zshrc /home/$user/ >> $output || exit 1

mkdir -p "$user_home/.zsh"
git clone https://github.com/sindresorhus/pure.git "$user_home/.zsh/pure"