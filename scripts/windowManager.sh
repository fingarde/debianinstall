#!/usr/bin/env bash
# -*- coding: utf-8 -*-

echo -e "\033[0;34mSetting up Window Manager\033[0;33m"

output="&1"
if [ ! -z ${use_script+x} ]; then mkdir logs 2> /dev/null; output="logs/window_manager.log"; fi

apt -y install xorg xinit i3 >> $output || exit 1
echo "exec i3" >> /etc/X11/xinit/xinitrc

ln -s $path/configs/kitty /home/$user/.config/kitty >> $output || exit 1