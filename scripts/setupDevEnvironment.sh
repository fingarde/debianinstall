#!/usr/bin/env bash
# -*- coding: utf-8 -*-

echo -e "\033[0;34mSetting up dev environment\033[0;33m"

output="&1"
if [ ! -z ${use_script+x} ]; then mkdir logs 2> /dev/null; output="logs/dev_env.log"; fi

installIdeaPlugin() {
    downloadId=$(curl -s "https://plugins.jetbrains.com/api/plugins/10044/updates?channel=&size=8" | jq -r ".[0]".id)
    wget -qO- "https://plugins.jetbrains.com/plugin/download?rel=true&updateId=$1" | bsdtar -xf- -C ~/.local/share/JetBrains/IntelliJIdea*
}

installVSCodePlugin() {
    code e--install-extension "$1" >> $output || exit 1
}


# VS Code
echo -e "\t> \033[0;34mInstalling VS Code\033[0;33m"

wget -qO- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > packages.microsoft.gpg
install -o root -g root -m 644 packages.microsoft.gpg /etc/apt/trusted.gpg.d/
sh -c 'echo "deb [arch=amd64 signed-by=/etc/apt/trusted.gpg.d/packages.microsoft.gpg] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list'

sudo apt-get install -y apt-transport-https
sudo apt-get update
sudo apt-get install -y code

echo -e "  \033[0;33m> \033[0;34mConfiguring VS Code\033[0;33m"

installVSCodePlugin kiteco.kite
installVSCodePlugin ms-vscode-remote.remote-ssh
installVSCodePlugin esbenp.prettier-vscode
installVSCodePlugin CoenraadS.bracket-pair-colorizer
installVSCodePlugin formulahendry.auto-rename-tag
installVSCodePlugin eamodio.gitlens
installVSCodePlugin donjayamanne.githistory
installVSCodePlugin pranaygp.vscode-css-peek
installVSCodePlugin xabikos.JavaScriptSnippets
installVSCodePlugin johnpapa.vscode-peacock
installVSCodePlugin ChakrounAnas.turbo-console-log
installVSCodePlugin wayou.vscode-todo-highlight
installVSCodePlugin vscode-icons-team.vscode-icons
installVSCodePlugin chrmarti.regex
installVSCodePlugin MS-vsliveshare.vsliveshare
installVSCodePlugin mtxr.sqltools
installVSCodePlugin ritwickdey.LiveServer
installVSCodePlugin christian-kohler.path-intellisense


# IntelliJ Idea
echo -e "\t> \033[0;34mInstalling IntelliJ Idea\033[0;33m"

sudo apt-get install -y curl jq bsdtar

ideaUrl=$(curl -s "https://data.services.jetbrains.com/products/releases?code=IIU&latest=true&type=release" | jq -r ".IIU[0].downloads.linux.link" || exit 1)
if [ "$ideaUrl" = "null" ]; then exit 1; fi
wget -qO- $ideaUrl | bsdtar -xf- >> $output || exit 1

mv idea-IU-* /opt/idea

echo -e "  \033[0;33m> \033[0;34mConfiguring IntelliJ Idea\033[0;33m"

installIdeaPlugin 10044
installIdeaPlugin 10321
installIdeaPlugin 10233
installIdeaPlugin 7638
installIdeaPlugin 7179
installIdeaPlugin 2162
installIdeaPlugin 10550
installIdeaPlugin 10080


# QT Creator
echo -e "  \033[0;33m> \033[0;34mInstalling QT Creator\033[0;33m"

apt install -y libqt* qtbase5-dev qtbase5-examples qtdeclarative5-dev qtdeclarative5-examples qtquickcontrols2-5-dev qtquickcontrols5-examples
apt install -y qtcreator


# Datagrip
echo -e "\t> \033[0;34mInstalling DataGrip\033[0;33m"

sudo apt-get install -y curl jq bsdtar

datagripUrl=$(curl -s "https://data.services.jetbrains.com/products/releases?code=DG&latest=true&type=release" | jq -r ".DG[0].downloads.linux.link" || exit 1)
if [ "$datagripUrl" = "null" ]; then exit 1; fi
wget -qO- $datagripUrl | bsdtar -xf- >> $output || exit 1

mv DataGrip-* /opt/datagrip


# CLion
echo -e "\t> \033[0;34mInstalling CLion\033[0;33m"

sudo apt-get install -y curl jq bsdtar

clionUrl=$(curl -s "https://data.services.jetbrains.com/products/releases?code=CL&latest=true&type=release" | jq -r ".CL[0].downloads.linux.link" || exit 1)
if [ "$clionUrl" = "null" ]; then exit 1; fi
wget -qO- $clionUrl | bsdtar -xf- >> $output || exit 1

mv clion-* /opt/clion

