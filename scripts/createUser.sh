#!/usr/bin/env bash
# -*- coding: utf-8 -*-

echo -e "\033[0;34mCreating user $user\033[0;33m"

output="&1"
if [ ! -z ${use_script+x} ]; then mkdir logs 2> /dev/null; output="logs/user.log"; fi

adduser $user >> $output || exit 1
adduser $user sudo || exit 1