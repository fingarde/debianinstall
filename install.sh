#!/usr/bin/env bash
# -*- coding: utf-8 -*-

export use_script=true

printf "\033[0;36mNew user: \033[0m"
read user

export path=$(pwd)
export user_home="/home/$user"
export user 

scripts/createUser.sh || ( echo -e "\n\033[0;31mError while creating user, program exit\033[0m"; exit 1 )
scripts/windowManager.sh  || ( echo -e "\n\033[0;31mError while isntalling window manager, program exit\033[0m"; exit 1 )
scripts/terminal.sh || ( echo -e "\n\033[0;31mError while installing terminal, program exit\033[0m";exit 1 )
scripts/setupDevEnvironment.sh || ( echo -e "\n\033[0;31mError while creating dev environment, program exit\033[0m"; exit 1 )

